<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>bar</title>

		<link rel="stylesheet" href="<?php base_url()?>/assets/css/estilo.css">
		<link rel="stylesheet" href="<?php base_url()?>/assets/stylee.css">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	</head>
	<body >
    <center>
    <nav class="navegacion-principal" >
        <a class="ping" href="<?php echo Site_url() ?>"> INICIO</a>
        <a class="ping" href="<?php echo Site_url('welcome/nosotros') ?>">NOSOTROS</a>
        <a class="ping" href="<?php echo Site_url('welcome/vision') ?>">VISION Y MISION</a>
        <a  class="ping" href="<?php echo Site_url('welcome/menu') ?>">MENU</a>
        <a class="ping" href="<?php echo Site_url('welcome/clientes') ?>">CLIENTES</a>
        <a class="ping" href="reservas.html">RESERVAS</a>
        <a class="ping" href="contactos.html">CONTACTOS</a>
        <a class="ping" href="ubicacion.html">UBICACION</a>
    </nav>
    </center>
